import { Router, Request, Response, NextFunction } from 'express';
import { CalendrierModel, Calendrier } from '../Models/calendrier_model';

export namespace CalendrierController {

    export async function getAll(req: Request, res: Response, next: NextFunction) {

        const results = await CalendrierModel.getAll();
        res.json(results);
    }

    export async function getOneById(req: Request, res: Response, next: NextFunction) {

        const results = await CalendrierModel.getOneById(req.params.id);
        res.json(results);
    }


    export async function getOneByCalendrierDate(req: Request, res: Response, next: NextFunction) {
        const results = await CalendrierModel.getOneByCalendrierDate(req.params.calendrier_date);
        res.json(results);
    }




    export async function createCalendrier(req: Request, res: Response, next: NextFunction) {
        try {
            const calendrier = new Calendrier(req.body);
            const results = await CalendrierModel.insertCalendrier(calendrier);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }


    export async function deleteCalendrier(req: Request, res: Response, next: NextFunction) {

        const results = await CalendrierModel.deleteCalendrierById(req.params.id);
        res.json(results);
    }
    // POUR UPDATE
    export async function updateCalendrierById(req: Request, res: Response, next: NextFunction) {
        try {
            const calendrier = new Calendrier(req.body);
            const results = await CalendrierModel.updateCalendrierById(req.params.id, calendrier);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }






}