import { Router, Request, Response, NextFunction } from 'express';
import { HoraireModel, Horaire } from '../Models/horaire_model';

export namespace HoraireController {

    export async function getAll(req: Request, res: Response, next: NextFunction) {

        const results = await HoraireModel.getAll();
        res.json(results);
    }

    export async function getOneById(req: Request, res: Response, next: NextFunction) {

        const results = await HoraireModel.getOneById(req.params.id);
        res.json(results);
    }

    export async function getOneByUserId(req: Request, res: Response, next: NextFunction) {
        const results = await HoraireModel.getOneByUserId(req.params.user_id);
        res.json(results);
    }

    export async function getOneByHeure(req: Request, res: Response, next: NextFunction) {
        const results = await HoraireModel.getOneByHeure(req.params.heure_id);
        res.json(results);
    }




    export async function getOneByCalendrierId(req: Request, res: Response, next: NextFunction) {
        const results = await HoraireModel.getOneByCalendrierId(req.params.calendrier_id);
        res.json(results);
    }




    export async function createHoraire(req: Request, res: Response, next: NextFunction) {
        try {
            const horaire = new Horaire(req.body);
            const results = await HoraireModel.insertHoraire(horaire);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }

    export async function createDate(req: Request, res: Response, next: NextFunction) {
        try {
            const horaire = new Horaire(req.body);
            const results = await HoraireModel.insertDate(horaire);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }


    export async function deleteHoraire(req: Request, res: Response, next: NextFunction) {

        const results = await HoraireModel.deleteHoraireById(req.params.id);
        res.json(results);
    }

    // POUR UPDATE
    export async function updateHoraireById(req: Request, res: Response, next: NextFunction) {
        try {
            const horaire = new Horaire(req.body);
            const results = await HoraireModel.updateHoraireById(req.params.id, horaire);
            res.json(results);
        } catch (err) {
            res.status(500).send(err);
        }
    }






}