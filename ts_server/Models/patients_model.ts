import { connect } from '../Connections/movie_db';

export class Patients {
    id: number;

    diagnostique: string;

    users_id: number;

    constructor(data: any) {
        this.id = data.id;

        this.diagnostique = data.diagnostique;

        this.users_id = data.users_id;

    }

}
export class PatientsModel {
    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM patients').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM patients WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async insertPatient(patient: Patients) {
        console.log(patient);
        return connect().then((conn) => {
            return conn.query('INSERT INTO patients ( diagnostique, users_id) VALUES (?,?)', [patient.diagnostique, patient.users_id]).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async deletePatientById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM patients WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }

    public static async updatePatientById(id: any, patient: Patients) {
        return connect().then((conn) => {
            return conn.query('UPDATE patients SET diagnostique=? WHERE id =?',
                [patient.diagnostique, id]).then((results) => {
                    return this.getAll();
                });
        });
    }
}