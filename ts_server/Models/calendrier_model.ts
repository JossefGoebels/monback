import { connect } from '../Connections/movie_db';


export class Calendrier {
    id: number;

    calendrier_date: string;

    constructor(data: any) {
        this.id = data.id;



        this.calendrier_date = data.calendrier_date;




    }

}
export class CalendrierModel {
    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM hrcalendrier').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM hrcalendrier WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async getOneByCalendrierDate(calendrier_date: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM hrcalendrier WHERE calendrier_date = ?', calendrier_date).then((results) => {
                return results;
            });
        });
    }


    public static async insertCalendrier(calendrier: Calendrier) {
        console.log(calendrier);
        return connect().then((conn) => {
            return conn.query('INSERT INTO hrcalendrier ( calendrier_date ) VALUES (?)', [calendrier.calendrier_date]).then((results) => {
                return this.getAll();
            });
        });
    }



    public static async deleteCalendrierById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM hrcalendrier WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }
    // POUR EDITER 
    public static async updateCalendrierById(id: any, calendrier: Calendrier) {
        return connect().then((conn) => {
            return conn.query('UPDATE hrcalendrier SET calendrier_date WHERE id =?',
                [calendrier.calendrier_date, id]).then((results) => {
                    return this.getOneById(id);
                });
        });
    }

    /*
 
     public static async updatePassword(user: Users) {
         return connect().then((conn) => {
             return conn.query('UPDATE users SET password=? WHERE id=?', [user.password, user.id]).then((results) => {
                 return this.getOneById(user.id);
             });
         });
     }
 */
}