import { connect } from '../Connections/movie_db';



export class Horaire {
    id: number;

    heure_id: number;
    calendrier_id: number;
    user_id: number;

    calendrier_date: string;
    heure: string;
    nom: string;
    prenom: string;


    constructor(data: any) {
        this.id = data.id;
        this.heure_id = data.heure_id;
        this.calendrier_id = data.calendrier_id;
        this.user_id = data.user_id;

        this.calendrier_date = data.calendrier_date;
        this.heure = data.heure;
        this.nom = data.nom;
        this.prenom = data.prenom;


    }

}
export class HoraireModel {
    /* public static async getAll() {
         return connect().then((conn) => {
             return conn.query('SELECT * FROM horaire').then((results) => {
                 return results;
             });
         });
     }*/


    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT H.id, HR.calendrier_date, HEU.heure, U.nom , U.prenom FROM horaire H JOIN hrcalendrier HR ON H.calendrier_id = HR.id JOIN heures HEU ON H.heure_id = HEU.id JOIN users U ON H.user_id = U.id').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM horaire WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async getOneByUserId(user_id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM horaire WHERE user_id = ?', user_id).then((results) => {
                return results;
            });
        });
    }


    public static async getOneByHeure(heure_id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM horaire WHERE heure_id=?', heure_id).then((results) => {
                return results;
            });
        })
    }

    public static async getOneByCalendrierId(calendrier_id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM horaire WHERE calendrier_id=?', calendrier_id).then((results) => {
                return results;
            });
        })
    }


    // calendrier id + user_id ? à inserer ici? 
    public static async insertHoraire(horaire: Horaire) {
        console.log(horaire);
        return connect().then((conn) => {
            return conn.query('INSERT INTO horaire ( heure_id, calendrier_id, user_id  ) VALUES (?,?,?)', [horaire.heure_id, horaire.calendrier_id, horaire.user_id]).then((results) => {
                return this.getAll();
            });
        });
    }
    //
    public static async insertDate(horaire: Horaire) {
        console.log(horaire);
        return connect().then((conn) => {
            return conn.query('INSERT INTO horaire (heure_id, calendrier_id, user_id) VALUES ((SELECT id FROM heures WHERE heure=?), (SELECT id FROM hrcalendrier WHERE calendrier_date =?), ? )', [horaire.heure, horaire.calendrier_date, horaire.user_id]).then((results) => {
                return this.getAll();
            });
        });
    }


    public static async deleteHoraireById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM horaire WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }
    // POUR EDITER 
    public static async updateHoraireById(id: any, horaire: Horaire) {
        return connect().then((conn) => {
            return conn.query('UPDATE horaire SET heure_id=?, calendrier_id=?, user_id=? WHERE id =?',
                [horaire.heure_id, horaire.calendrier_id, horaire.user_id, id]).then((results) => {
                    return this.getOneById(id);
                });
        });
    }





}