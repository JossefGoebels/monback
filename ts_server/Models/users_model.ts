import { connect } from '../Connections/movie_db';

export class Users {
    id: number;
    // username: string;
    email: string;
    password: string;
    admin: boolean;

    nom: string;
    prenom: string;
    adresse: string;
    date_naissance: string;
    tel: string;

    constructor(data: any) {
        this.id = data.id;
        // this.username = data.username;
        this.email = data.email;
        this.password = data.password;
        this.admin = data.admin ? data.admin : 0;
        this.nom = data.nom;
        this.prenom = data.prenom;
        this.adresse = data.adresse;
        this.date_naissance = data.date_naissance;
        this.tel = data.tel;

    }

}
export class UsersModel {
    public static async getAll() {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM users').then((results) => {
                return results;
            });
        });
    }

    public static async getOneById(id: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM users WHERE id = ?', id).then((results) => {
                return results;
            });
        });
    }

    public static async getOneByName(nom: any) {
        return connect().then((conn) => {
            return conn.query('SELECT * FROM users WHERE nom = ?', nom).then((results) => {
                return results;
            });
        });
    }


    public static async getOneByEmail(email: any) {
        return connect().then((conn) => {
            return conn.query('SELECT id, email, admin, nom, prenom, adresse, date_naissance, tel FROM users WHERE email=?', email).then((results) => {
                return results;
            });
        })
    }


    /*  public static async insertUserProfile(user: Users) {
          console.log(user);
          return connect().then((conn) => {
              return conn.query('INSERT INTO users ( nom, prenom, adresse, date_naissance, tel ) VALUES (?,?,?,?,?)', [user.nom, user.prenom, user.adresse, user.date_naissance, user.tel]).then((results) => {
                  return this.getAll();
              });
          });
      }          */

    public static async insertUser(user: Users) {
        console.log(user);
        return connect().then((conn) => {
            return conn.query('INSERT INTO users ( email, password ) VALUES (?,?)', [user.email, user.password]).then((results) => {
                return this.getAll();
            });
        });
    }



    public static async deleteUserById(id: any) {
        return connect().then((conn) => {
            return conn.query('DELETE FROM users WHERE id = ?', id).then((results) => {
                return this.getAll();
            });
        });
    }
    // POUR EDITER PROFIL
    public static async updateUserById(id: any, user: Users) {
        return connect().then((conn) => {
            return conn.query('UPDATE users SET nom=?, prenom=?, adresse=?, date_naissance=?, tel=? WHERE id =?',
                [user.nom, user.prenom, user.adresse, user.date_naissance, user.tel, id]).then((results) => {
                    return this.getOneById(id);
                });
        });
    }

    public static async checkPassword(email: string, password: string): Promise<any> {
        let res = await connect().then((conn) => {
            return conn.query('SELECT id, email, password, admin FROM users WHERE email=?', email).then((results) => {
                return results;
            });
        });
        if (res[0].password === password) {
            return { success: true, admin: res[0].admin, id: res[0].id };
        } return { success: false, admin: false };
    }

    public static async updatePassword(user: Users) {
        return connect().then((conn) => {
            return conn.query('UPDATE users SET password=? WHERE id=?', [user.password, user.id]).then((results) => {
                return this.getOneById(user.id);
            });
        });
    }

}