import { Router } from 'express';
import { HoraireController } from '../Controllers/horaires_controller';
import { CalendrierController } from '../Controllers/calendrier_controller';

export class CalendrierHoraireRouter {
    public router: Router;
    public authRouter: Router;

    constructor() {
        this.router = Router();

        this.router.get('/horaire', HoraireController.getAll);
        this.router.get('/horaire/:id', HoraireController.getOneById);
        this.router.get('/horaire/user/:user_id', HoraireController.getOneByUserId);
        this.router.get('/horaire/heure/:heure_id', HoraireController.getOneByHeure);
        this.router.get('/horaire/calendrier/:calendrier_id', HoraireController.getOneByCalendrierId);
        this.router.post('/horaire/create', HoraireController.createHoraire);
        this.router.post('/horaire/createdate', HoraireController.createDate);
        this.router.delete('/horaire/:id', HoraireController.deleteHoraire);
        this.router.put('/horaire/:id', HoraireController.updateHoraireById);


        this.router.get('/calendrier', CalendrierController.getAll);
        this.router.get('/calendrier/:id', CalendrierController.getOneById);
        this.router.get('/calendrier/date/:calendrier_date', CalendrierController.getOneByCalendrierDate);
        this.router.post('/calendrier/create', CalendrierController.createCalendrier);
        this.router.delete('/calendrier/:id', CalendrierController.deleteCalendrier);
        this.router.put('/calendrier/:id', CalendrierController.updateCalendrierById);

        // FAIRE LES ROLES ET AUTHORISATION  A LA FIN !!!!

        this.authRouter = Router();









    }
}