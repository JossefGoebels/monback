import { Router } from 'express';
import { PatientsController } from '../controllers/patients_controller';


export class PatientsRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.router.get('/patients', PatientsController.getAll);
        this.router.get('/patients/id/:id', PatientsController.getOneById);
        this.router.post('/patients/create', PatientsController.createPatient);
        this.router.delete('/patients/:id', PatientsController.deletePatient);
        this.router.put('/patients/:id', PatientsController.updatePatientById);

    }
}