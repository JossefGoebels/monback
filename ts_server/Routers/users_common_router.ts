
import { Router } from 'express';
import { UsersController } from '../Controllers/users_controller'

export class UsersCommonRouter {
    public router: Router;
    public authRouter: Router;

    constructor() {
        this.router = Router();



        this.authRouter = Router();

        // this.router.get('/users', UsersController.getAll);

        //this.authRouter.put('users/password', UsersController.updateUserPassword);

        // this.authRouter.put('/users/:id', UsersController.updateConnectedUser); 

        //this.router.delete('/users/:id', UsersController.deleteUser);
        //this.router.put('/users/:id', UsersController.updateUserById);


        // FAIRE LES ROLES ET AUTHORISATION  A LA FIN !!!!


        //this.router.post('/users/create', UsersController.createUser);
        this.router.get('/users/:name', UsersController.getOneByName);

        this.router.get('/users', UsersController.getAll);

        this.router.put('users/password', UsersController.updateUserPassword);

        //this.router.put('/users/:id', UsersController.updateConnectedUser);

        this.router.delete('/users/:id', UsersController.deleteUser);
        this.router.put('/users/:id', UsersController.updateUserById);
    }
}