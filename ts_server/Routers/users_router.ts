import { Router } from 'express';
import { UsersController } from '../controllers/users_controller';


export class UsersRouter {
    public router: Router;

    constructor() {
        this.router = Router();











        this.router.get('/users/email/:email', UsersController.getOneByEmail);

        this.router.get('/users/id/:id', UsersController.getOneById);

        this.router.post('/users/create', UsersController.createUser);



    }
}